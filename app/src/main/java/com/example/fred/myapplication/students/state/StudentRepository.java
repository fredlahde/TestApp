package com.example.fred.myapplication.students.state;

import android.support.annotation.NonNull;

import com.example.fred.myapplication.students.factorys.StudentFactory;
import com.example.fred.myapplication.students.models.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentRepository {
    private static short Id;
    final private List<Student> studentList;

    public static short getId() {
        return Id;
    }

    public StudentRepository(final Student... students) {
        this.studentList = new ArrayList<>();
        Id++;

        Collections.addAll(this.studentList, students);
    }

    public StudentRepository(List<Student> studentList) {
        this.studentList = studentList;
    }

    public void add(Student student) {
        this.studentList.add(student);
    }


    public List<Student> getStudentList() {
        return studentList;
    }

    public Student get(int i) {
        return this.studentList.get(i);
    }

    public List<Student> getWithName(@NonNull final String name) {
        List<Student> studentListWithName = new ArrayList<>();
        for (Student student : studentList)
            if (student.getName().equalsIgnoreCase(name)) studentListWithName.add(student);

        return studentListWithName;
    }

    public void clear() {
        this.studentList.clear();
    }

    public int count() {
        return this.studentList.size();
    }

    public void add() {
        this.studentList.add(StudentFactory.getInstance().makeNewStudent());

    }

}

package com.example.fred.myapplication.students.models;

/**
 * Created by fred on 8/10/2016.
 */

public class Student {
    private int age;
    final private String name;
    private Adress adress;

    public Student(int age, String name, Adress adress) {
        this.age = age;
        this.name = name;
        this.adress = adress;
    }

    public Student(int age, String name, String street, int number) {
        this(age, name, new Adress(street, number));
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Adress getAdress() {
        return adress;
    }

    @Override
    public String toString() {
        return name;
    }
}

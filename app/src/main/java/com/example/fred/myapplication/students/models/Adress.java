package com.example.fred.myapplication.students.models;

public class Adress {
    private String street;
    private int number;

    public Adress(String street, int number) {
        this.street = street;
        this.number = number;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "street='" + street + '\'' +
                ", number=" + number +
                '}';
    }
}
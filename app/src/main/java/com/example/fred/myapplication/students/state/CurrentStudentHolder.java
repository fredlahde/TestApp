package com.example.fred.myapplication.students.state;

import com.example.fred.myapplication.students.models.Student;

public class CurrentStudentHolder {

    private Student currentStudent;
    private static CurrentStudentHolder ourInstance = new CurrentStudentHolder();

    public static CurrentStudentHolder getInstance() {
        return ourInstance;
    }

    private CurrentStudentHolder() {
    }

    public Student getCurrentStudent() {
        return currentStudent;
    }

    public void setCurrentStudent(Student currentStudent) {
        this.currentStudent = currentStudent;
    }
}

package com.example.fred.myapplication.students.factorys;

import com.example.fred.myapplication.students.models.Student;

import java.util.concurrent.ThreadLocalRandom;

public class StudentFactory {
    private static StudentFactory ourInstance = new StudentFactory();

    public static StudentFactory getInstance() {
        return ourInstance;
    }

    private final String[] NAMES = new String[]{
            "Jonas", "Max", "Herbert", "Markus", "Gustav", "Karl", "Otto", "Peter"
    };
    private final String[] STREETS = new String[]{
            "Meyer Straße", "Dumpo Weg", "Hauptstraße"
    };

    private StudentFactory() {
    }

    private String getStreet() {

        return STREETS[ThreadLocalRandom.current().nextInt(0, STREETS.length)];
    }

    private int getStreetNumber() {
        return ThreadLocalRandom.current().nextInt(1, 24);
    }

    private int getAge() {
        return ThreadLocalRandom.current().nextInt(1, 50);
    }
    private String getName() {

        return NAMES[ThreadLocalRandom.current().nextInt(0, NAMES.length)];

    }

    public Student makeNewStudent() {
        return new Student(
                getAge(), getName(), getStreet(), getStreetNumber()
        );
    }
}

package com.example.fred.myapplication.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.example.fred.myapplication.students.models.Student;
import com.example.fred.myapplication.students.state.CurrentStudentHolder;
import com.example.fred.myapplication.students.state.StudentRepository;

/**
 * Created by fred on 8/11/2016.
 */

public class StudentListItemListener implements AdapterView.OnItemClickListener {
    private static final String TAG =  StudentListItemListener.class.getSimpleName();
    private final StudentRepository STUDENTREPO;
    private final MainActivity MAIN_ACTIVITY;

    public StudentListItemListener(StudentRepository studentRepository, MainActivity MAIN_ACTIVITY) {
        this.STUDENTREPO = studentRepository;
        this.MAIN_ACTIVITY = MAIN_ACTIVITY;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Student s = STUDENTREPO.get(i);
        CurrentStudentHolder.getInstance().setCurrentStudent(s);
        Intent intent = new Intent(MAIN_ACTIVITY, Main2Activity.class);
        MAIN_ACTIVITY.startActivity(intent);
    }
}

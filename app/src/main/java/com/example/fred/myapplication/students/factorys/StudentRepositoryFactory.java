package com.example.fred.myapplication.students.factorys;

import android.support.annotation.NonNull;

import com.example.fred.myapplication.students.state.StudentRepository;

public class StudentRepositoryFactory {


    private static StudentRepositoryFactory instance;
    private StudentRepository studentRepository;

    public static StudentRepositoryFactory getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new StudentRepositoryFactory();
        return instance;
    }

    @NonNull
    public StudentRepository getStudentRepository(final int numberOfStudents) {
        if (studentRepository == null) {
            studentRepository = new StudentRepository();
        }
        for (int i = 0; i < numberOfStudents; i++) {
            studentRepository.add(StudentFactory.getInstance().makeNewStudent());
        }
        return studentRepository;
    }


}

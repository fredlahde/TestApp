package com.example.fred.myapplication.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import com.example.fred.myapplication.R;
import com.example.fred.myapplication.students.factorys.StudentRepositoryFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private final MainActivity mainActivity = this;
    protected ListView listView;
    protected Button btnAdd, btnClear;
    protected final StudentRepositoryFactory studentRepositoryFactory = StudentRepositoryFactory.getInstance();
    private MainActivityController mainActivityController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityController = new MainActivityController(this);
        listView = (ListView) findViewById(R.id.listView);
        btnAdd = ((Button) findViewById(R.id.btnAdd));
        btnClear = ((Button) findViewById(R.id.btnClear));



        mainActivityController.buildView();

    }
}

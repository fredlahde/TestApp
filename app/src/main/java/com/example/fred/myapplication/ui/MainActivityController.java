package com.example.fred.myapplication.ui;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.example.fred.myapplication.R;
import com.example.fred.myapplication.students.models.Student;
import com.example.fred.myapplication.students.state.StudentRepository;

import java.util.List;

/**
 * Created by fred on 8/11/2016.
 */

public class MainActivityController {
    private final static String TAG = MainActivityController.class.getSimpleName();
    private final MainActivity MAIN_ACTIVITY;

    private final static class ButtonListener implements View.OnClickListener {

        private final static String TAG = ButtonListener.class.getSimpleName();
        private final StudentRepository studentRepository;
        private final MainActivity MAIN_ACTIVITY;


        private ButtonListener(StudentRepository studentRepository, MainActivity mainActivity) {
            this.studentRepository = studentRepository;
            this.MAIN_ACTIVITY = mainActivity;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnAdd:
                    Log.d(TAG, "Pressed Button Add");
                    addStudentToRepo();
                    break;

                case R.id.btnClear:
                    Log.d(TAG, "Pressed Button Clear");
                    clearStudentRepo();
                    break;
            }
        }

        private void clearStudentRepo() {
            studentRepository.clear();
            updateListView();
        }

        private void addStudentToRepo() {
            studentRepository.add();
            updateListView();
        }

        public void updateListView() {
            setListAdapter();
            Log.d(TAG, "List updated");
        }

        private void setListAdapter() {
            final List<Student> studentList = studentRepository.getStudentList();

            MAIN_ACTIVITY.listView.setAdapter(new ArrayAdapter<>(
                    MAIN_ACTIVITY,
                    R.layout.activity_listview,
                    studentList));
            Log.d(TAG, "ListAdapter set");

        }
    }

    public MainActivityController(MainActivity MAIN_ACTIVITY) {
        this.MAIN_ACTIVITY = MAIN_ACTIVITY;
    }

    public void buildView() {

        final StudentRepository studentRepository = MAIN_ACTIVITY.studentRepositoryFactory.
                getStudentRepository(0);


        final ButtonListener buttonListener = new ButtonListener(studentRepository, MAIN_ACTIVITY);

        MAIN_ACTIVITY.listView.setOnItemClickListener(
                new StudentListItemListener(studentRepository, MAIN_ACTIVITY
                ));


        MAIN_ACTIVITY.btnAdd.setOnClickListener(buttonListener);

        MAIN_ACTIVITY.btnClear.setOnClickListener(buttonListener);

    }


}

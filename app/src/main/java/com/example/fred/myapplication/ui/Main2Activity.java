package com.example.fred.myapplication.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.fred.myapplication.R;
import com.example.fred.myapplication.students.models.Student;
import com.example.fred.myapplication.students.state.CurrentStudentHolder;

public class Main2Activity extends AppCompatActivity {


    private TextView txtAge;
    private TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        txtName = ((TextView) findViewById(R.id.txtName));
        txtAge = ((TextView) findViewById(R.id.txtAge));

        final Student currStudent = CurrentStudentHolder.getInstance().getCurrentStudent();
        txtName.setText(currStudent.getName());
        txtAge.setText(String.valueOf(currStudent.getAge()));

    }

}

package com.example.fred.myapplication;

import com.example.fred.myapplication.students.factorys.StudentFactory;
import com.example.fred.myapplication.students.factorys.StudentRepositoryFactory;
import com.example.fred.myapplication.students.models.Student;
import com.example.fred.myapplication.students.state.CurrentStudentHolder;
import com.example.fred.myapplication.students.state.StudentRepository;

import org.junit.Test;

import static org.junit.Assert.*;

public class StudentTest {

    @Test
    public void it_can_generate_a_student() {
        final Student s = StudentFactory.getInstance().makeNewStudent();
        assertNotNull(s);
        assertNotNull(s.getAdress());
    }

    @Test
    public void it_can_generate_a_student_repository() {
        final StudentRepository sr = StudentRepositoryFactory.getInstance().getStudentRepository(0);
        assertNotNull(sr);
    }

    @Test
    public void it_is_only_one_student_repo_at_a_time() {
        final StudentRepository sr = StudentRepositoryFactory.getInstance().getStudentRepository(0);
        final StudentRepository sr2 = StudentRepositoryFactory.getInstance().getStudentRepository(0);
        assertEquals(1, StudentRepository.getId());
    }

    @Test
    public void it_can_hold_a_student() {
        final Student s = StudentFactory.getInstance().makeNewStudent();

        final CurrentStudentHolder holder = CurrentStudentHolder.getInstance();
        holder.setCurrentStudent(s);

        assertEquals(s, holder.getCurrentStudent());
    }
}